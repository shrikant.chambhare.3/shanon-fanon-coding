#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>
typedef struct cha{
	int ch;
	float count;
}chara;
char str[128][10];
char *ONE = "1";
char *ZERO = "0";
int comparator(const void *p, const void *q)  { 
	if(((chara *)p)->count > (( chara *)q)->count)
		return 0;
	return 1;
} 


float arr_sum(int n, chara *arr){
	int i;
	float sum = 0;
	for(i = 0; i < n; i++){
		sum += arr[i].count;
	}
	return sum;
}

void  make_division(chara *arr, int f, int l){
	float total, half;
	float tmp = 0, temp = 0;
	int i;
	if(f >= l){
		//printf("\n");
		return;
	}
	total = arr_sum(l-f+1, arr+f);
	//printf("%f\n", total);
	half = total / 2.0;
	for(i = f; i <= l ; i++){
		tmp += arr[i].count;
		if(tmp >= half)
			break;
	}
	temp = tmp - arr[i].count;
	if((2 * tmp - total) <= (total- 2 * temp)){
		for(int k=f; k <= i; k++)
			strcat(str[k], ONE);
		make_division(arr, f, i);
		for(int k = i+1; k <= l; k++)
			strcat(str[k], ZERO);
		make_division(arr, i+1, l);		
	}
	else{
		for(int k=f; k <= i-1; k++)
			strcat(str[k], ONE);
		make_division(arr, f,i-1);
		for(int k = i; k <= l; k++)
			strcat(str[k], ZERO);
		make_division(arr, i, l);
			
	}
}

int main(int argc, char *argv[]){
	int fd, n, i, k;
	chara arr[128];
	for(i = 0; i < 128; i++){
		arr[i].ch = i;
		arr[i].count = 0.0;
	}
	char ch;
	if(argc < 2){
		perror("Enter file name as command line argument");
		exit(EXIT_FAILURE);
	}
	fd = open(argv[1], O_RDONLY);
	while(read(fd, &ch, 1) != 0){
	//	printf("%c", ch);
		arr[ch-'\0'].count += 1;
	}
//	for(int i = 0; i < 128; i++){
//		printf("%c: %f\n", arr[i].ch, arr[i].count);
//	}
	
	float sum = arr_sum(128, arr);
//	printf("%f", sum);
	qsort((void *)arr, 128, sizeof(chara), comparator);
/*	for(i = 0; i <128; i++){
		printf("%c: %f\n", arr[i].ch, arr[i].count);
	}	
*/	for(k = 0; k < 128 && arr[k].count != 0.0; k++){
		arr[k].count = arr[k].count / sum;
	}
	n = k;
	printf("Probability of characters:\n");
	for(i = 0; i < n; i++){
		printf("%c: %f\n", arr[i].ch, arr[i].count);
	}
	
	make_division(arr, 0, n-1);
	printf("SHANON-FANON CODE:\n");
	for(i = 0; i < n; i++){
		printf("%c: %s\n", arr[i].ch, str[i]);
	}
	return 0;
}


