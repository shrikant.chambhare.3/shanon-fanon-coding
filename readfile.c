#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>
int *readfile(int argc, char *argv[]){
	int fd;
	int arr[128] = {0};
	char ch;
	if(argc < 2){
		perror("Enter file name as command line argument");
		exit(EXIT_FAILURE);
	}
	fd = open(argv[1], O_RDONLY);
	while(read(fd, &ch, 1) != 0){
	//	printf("%c", ch);
		arr[ch-'\0'] += 1;
	}
	/*for(int i = 0; i < 128; i++){
		printf("%c: %d\n", i, arr[i]);
	}
	*/
	return 0;
}
